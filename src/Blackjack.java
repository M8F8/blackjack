import java.util.Scanner;
import java.util.Random;

public class Blackjack {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Random randomCard = new Random();

		String hitOrStay = "";
		String goAgain = "";

		do {
			// Variabler som hĺller kollar pĺ spelaren och dealerns resultat.
			int player = 0;
			int dealer = 0;

			System.out.println("Dags för lite blackjack!");

			do {
				// Detta sköter den slumpmässiga utdelningen av korten.
				player += randomCard.nextInt(13) + 1;
				System.out.println("Din poäng: " + player);

				if (dealer < 16) {
					dealer += randomCard.nextInt(13) + 1;
				}

				System.out.println("Dealerns poäng: " + dealer);
				// Här bryts loopen om spelaren eller dealern kommer över 21
				if (player >= 21)
					break;
				if (dealer >= 21)
					break;

				System.out.println("Hit or stay? H/S");
				hitOrStay = input.next();

			}
			// Här fĺr spelaren välja om hen vill dra ett till kort
			while (hitOrStay.equalsIgnoreCase("h"));

			while (dealer < 16 && player <= 21) {
				dealer += randomCard.nextInt(13) + 1;
				System.out.println("Dealerns poäng: " + dealer);
			}

			// Följande if/else if stämmer av vad som skall skrivas ut beroende
			// pĺ vilket resultat man har efter loopen har brutits
			if ((player < dealer && dealer <= 21) || (player > 21)) {
				System.out.println("Dealern vann.");
			}

			else if (player == dealer) {
				System.out.println("Det blev oavgjort.");
			}

			else if ((player > dealer && player <= 21) || (dealer > 21 && player < 21)) {
				System.out.println("Du vann!");
			}

			// Här fĺr spelaren välja om hen vill spela en omgĺng till.
			System.out.println("Vill du spela en gĺng till? Y/N");
			goAgain = input.next();

		} while (goAgain.equalsIgnoreCase("y"));
		input.close();

	}

}
